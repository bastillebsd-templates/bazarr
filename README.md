## Bazarr

Bazarr is a companion application to Sonarr and Radarr that manages and downloads subtitles based on your requirements.

## Bootstrap

```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/bazarr
```

## Usage

```shell
bastille template TARGET bastillebsd-templates/bazarr
```

### Available arguments `--arg NAME=value`
- USER=bazarr
- GROUP=bazarr
- DATA_DIR=/usr/local/bazarr
- OUT_PORT=6767

For more options edit `/usr/local/bastille/templates/bastillebsd-templates/bazarr/Bastillefile`
